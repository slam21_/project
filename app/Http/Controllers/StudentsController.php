<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    
    public function index()
    {
        $students=Student::all();
        return view('students.index', compact('students'));
    }

    public function create(Request $request)
    {
        Student::create($request->all());
        return redirect('students/index')->with('success', 'Data Berhasil Ditambahkan');
    }

    public function edit(Student $id)
    {
        $students = Student::find($id);
        return view('students/edit', compact('students'));
    }

    public function update(Request $request, $id)
    {
        $students = Student::find($id);
        $students->update($request->all());
        return redirect('students/index')->with('success', 'Data Berhasil Diubah');
    }

    public function delete($id)
    {
        $students = Student::find($id);
        $students->delete();
        return redirect('students/index')->with('danger', 'Data Berhasil Dihapus');
    }
    public function store(Request $request)
    {
        //
    }

    public function show(Student $student)
    {
        //
    }
   
    public function destroy(Student $student)
    {
        //
    }
}
