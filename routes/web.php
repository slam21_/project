<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/dashboard/index', 'DashboardsController@index');

Route::get('/students/index', 'StudentsController@index');
Route::post('/students/create', 'StudentsController@create');
Route::get('/students/{id}/edit', 'StudentsController@edit');
Route::post('/students/{id}/update', 'StudentsController@update');
Route::get('/students/{id}/delete', 'StudentsController@delete');