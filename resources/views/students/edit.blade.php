@extends('layouts.master')
{{-- {{dd($student)}} --}}
@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Edit Data Students</h3>
                                    </div>
                                    @if (session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{session('success')}}
                                      </div>
                                    @endif
                                    @foreach ($students as $student)
                                    <form action="/students/{{$student->id}}/update" method="POST">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama Lengkap</label>
                                        <input name="nama" type="text" class="form-control" id="nama" aria-describedby="nama" placeholder="Masukkan Nama Lengkap" value="{{$student->nama}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                            <select name="kelamin" class="form-control" id="exampleFormControlSelect1">
                                              <option value="L" @if ($student->kelamin == 'L') selected @endif>Laki-laki</option>
                                              <option value="P" @if ($student->kelamin == 'P') selected @endif>Perempuan</option>
                                            </select>
                                          </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input name="email" type="text" class="form-control" id="email" aria-describedby="email" placeholder="Masukkan Email" value="{{$student->email}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Alamat</label>
                                            <textarea name="alamat" class="form-control" id="alamat" rows="3" placeholder="Masukkan Alamat" >{{$student->alamat}}</textarea>
                                        </div>
                                        <button type="submit" class="btn btn-warning">Ubah</button>
                                    </form>
                                    @endforeach
                                </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection