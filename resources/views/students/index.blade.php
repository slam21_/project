@extends('layouts.master')
{{-- {{dd($student)}} --}}
@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                                <!-- TABLE HOVER -->
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Data Students</h3>
                                        <div class="right">
                                            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal">
                                                Tambah Data!
                                            <i class="lnr lnr-plus-circle"></i></button>
                                        </div>
                                    </div>
                                    
                                    @if (session('success'))
                                    <div class="alert alert-success" role="alert">
                                        {{session('success')}}
                                      </div>
                                    @endif
                                    @if (session('danger'))
                                    <div class="alert alert-danger" role="alert">
                                        {{session('danger')}}
                                      </div>
                                    @endif
                                    <!-- Button trigger modal -->
                                        
                                    
                                    <div class="panel-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr><th>#</th>
                                                    <th>Nama Lengkap</th>
                                                    <th>Jenis Kelamin</th>
                                                    <th>Email</th>
                                                    <th>Alamat</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($students as $student)
                                                <tr>
                                                    <td>{{$student->id}}</td>
                                                    <td>{{$student->nama}}</td>
                                                    <td>{{$student->kelamin}}</td>
                                                    <td>{{$student->email}}</td>
                                                    <td>{{$student->alamat}}</td>
                                                    <td><a href="/students/{{$student->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                        <a href="/students/{{$student->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('yakin data mau dihapus?')">Delete</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END TABLE HOVER -->
                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h2 class="modal-title" id="exampleModalLabel">Tambah Data</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="/students/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Lengkap</label>
                        <input name="nama" type="text" class="form-control" id="nama" aria-describedby="nama" placeholder="Masukkan Nama Lengkap">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                        <select name="kelamin" class="form-control" id="exampleFormControlSelect1">
                          <option value="L">Laki-laki</option>
                          <option value="P" >Perempuan</option>
                        </select>
                      </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" type="text" class="form-control" id="email" aria-describedby="email" placeholder="Masukkan Email">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Alamat</label>
                        <textarea name="alamat" class="form-control" id="alamat" rows="3" placeholder="Masukkan Alamat"></textarea>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        </div>
    </div>
@endsection